library(MASS)
library(psych)
library(coda)
options(warn = -1)

InitVals_ <- function() {
  " n <- dim(X_full)[1];
  if (intercept) { X_full <- cbind(rep(1,n), X_full) }
  p <- dim(X_full)[2]; XtX <- t(X_full) %*% X_full
  betas <- matrix(0, nIter, p)
  gammas <- matrix(1, nIter, p)
  sigmas <- matrix(0, nIter, 1)
  lambdas <- matrix(0, nIter, 1)
  residus <- matrix(NA, nIter, 1)
  probgams <- matrix(NA, nIter, 1)


  a0 <- 0; b0 <- 0; a1 <- 0; b1 <- 0
  m0 <- eval_m(y, X_full, XtX, d)

  sigSq0 <- var(y)
  gamma0 <- rep(1,p)
  W0 = inv(evalWinv(d, XtX, ld0))
  B0 <- mvrnorm(1, m0, W0);
  sigmas[1] <- sigSq0;
  lambdas[1] <- ld0;
  betas[1,] <- B0;
  gammas[1,] <- gamma0;

  X <- X_full; B <- B0
  m <- m0; p_ <- p
  ld <- ld0; xtx <- XtX "
}

dataloader_ <- function() {
  "y = data$y
  X = data$X
  betas = data$betas
  tgs = data$tgs"
}

inv <- function(x) { solve(x) } #inverse

evalWinv <- function(d, xtx, ld) {
  p <- getDim(xtx)[2]
  res <- ld * xtx + d * ld * diag(p)
  res
}

evalWould <- function(d, xtx) {
  p <- getDim(xtx)[2]
  res <- xtx + d * diag(p)
  res
}

eval_m <- function(y, X, xtx, d) {
  p = getDim(xtx)[2]
  inv(xtx + d*diag(p)) %*% t(X) %*% y
}

eval_mstar <- function(y, X, xtx, d){
  #
}

resEval <- function(u) {
  t(u) %*% (u)
}

phi <- function(u, M, v = "", noInv = FALSE) {
  check = FALSE
  if (length(v) == 1) check = v == ""
  if (check & !noInv) { t(u) %*% inv(M) %*% u }
  else if (check & noInv) { t(u) %*% M %*% u }
  else if (noInv) { t(u) %*% M %*% v}
  else {  t(u) %*% inv(M) %*% v }
}

probGamma <- function(ld, sigSq, d, m, y, X, xtx){
  n <- getDim(X)[1]; V <- sigSq * diag(n); Winv <- evalWinv(d, xtx, ld)
  W <- inv(Winv); W_ <- inv(Winv +  phi(X, V))
  m_ <- W_ %*% (Winv %*% m + phi(X, V, y))
  e = exp( -0.5 * (-phi(m_, W_) + phi(y, V) + phi(m, Winv, noInv = TRUE)))
  p <- (2*pi*sigSq)^(-n/2) * det(Winv)^0.5*det(W_)^0.5 * e
}

logProgGamma <- function(ld, sigSq, d, m, y, X, xtx) {
  n <- getDim(X)[1]; V <- sigSq * diag(n); Winv <- evalWinv(d, xtx, ld)
  W <- inv(Winv); W_ <- inv(Winv +  phi(X, V))
  m_ <- W_ %*% (Winv %*% m + phi(X, V, y))
  le = -0.5 * (-phi(m_, W_) + phi(m, Winv, noInv = TRUE))
  lp <- 0.5*(log(det(Winv))+ log(det(W_))) + le
}

selectX <- function(X, gamma) {
  if (sum(gamma) == dim(X)[2]) X
  else if (sum(gamma) == 1) as.matrix(X[,which(gamma == 1)])
  else if (sum(gamma) == 0) { print("Selection Error: No variables selected") }
  else { X[,which(gamma == 1)] }
}

selectXtX <- function(gamma, XtX) {
  index = which(gamma == 1)
  if (sum(gamma) == dim(XtX)[1]) XtX
  else if (sum(gamma) == 1) as.matrix(XtX[index,index])
  else if (sum(gamma) == 0) { print("Selection Error for XtX: No correlations selected") }
  else { XtX[index,index] }
}

getDim <- function(X) {
  d <- dim(X)
  n = if (is.null(d)) {length(X)} else d[1]
  p = if (is.null(d)) {1} else d[2]
  c(n,p)
}

dDist <- function(d) {
  b_ = 0; a_ = 0; p = getDim(X)[2]
  xtx = t(X) %*% X
  deter = det(xtx + d*diag(p))
  lxp = -ld*d * resEval(B - m) - b_*d
  0.5 * log(deter) + lxp + (a_ - 1) * log(d)
}

dlog <- function(d) {
  logProgGamma(ld, sigSq, d, m, y, X)
}

alog <- function(d) {
  logProgGamma(ld, sigSq, d, m_alt, y, X_alt)
}

