
mcmc_maker <- function(results, pSampler = NULL, exclud = NULL, stp = 1) {
  library(coda)
  #Returns a list of mcmc objects
  dims <- dim(results$betas)
  p = dims[2]; n = dims[1]
  if (is.null(pSampler)) pSampler <- sample(1:p, 4)

  betas = results$betas[seq(1,n,stp),pSampler]
  gammas = results$gammas[seq(1,n,stp),pSampler]
  lambdas = results$lambdas[seq(1,n,stp)]
  sigmas = results$sigmas[seq(1,n,stp)]

  res <- list(lambdas = mcmc(lambdas), sigmas = mcmc(sigmas), betas = mcmc(betas), gammas = mcmc(gammas))
}

summary.bayes <- function(results, pSampler = NULL, vars= "all", exclud = NULL) {
  res <- mcmc_maker(results, pSampler, exclud)
  if (vars == "all") {
    for (el in res) {
      summary(el)
    } }
  else summary(res[[vars]])
  }


plot.bayes <- function(results, pSampler = NULL, vars = "all", stp = 1, exclud = NULL) {
  res <- mcmc_maker(results, pSampler, exclud, stp)
  if (vars == "all") {
    for (el in res) {
      plot(el)
    } }
  else plot(res[[vars]], main = vars)
}

gafreq <- function(results, pSampler = NULL, exclud = NULL) {
  p = dim(results$gammas)[2]
  if(is.null(pSampler)) pSampler = 1:p
  res <- mcmc_maker(results, pSampler, exclud)
  probs <- colMeans(res$gammas)
  plot(1:p, probs)
  segments(1:p, 0, 1:p, probs)
}

befreq <- function(gams) {
  p = dim(gams)[2]
  probs <- colMeans(gams)
  #p = length(probs)
  plot(1:p, probs)
  segments(1:p, 0, 1:p, probs)
}


histoplot <- function(results, pSampler = c(0)) {

  p <- dim(results$betas)[2]
  if (sum(pSampler) == 0) pSampler <- sample(1:p, 4)

  betas = results$betas[,pSampler]
  gammas = results$gammas[,pSampler]
  lamdas = results$lambdas
  sigmas = results$sigmas
  b1 = pSampler[1]; b2 = pSampler[2]; b3 = pSampler[3]; b4 = pSampler[4]
  minB = apply(betas, 2, min); maxB = apply(betas, 2, max)
  minL = min(lamdas); maxL = max(lamdas)
  minS = min(sigmas); maxS = max(sigmas)

  par(mfrow=c(2, 2))
  truehist(betas[,b1], xlim=c(1.25*minB[b1], 1.25*maxB[b1]))
  truehist(betas[,b2], xlim=c(1.25*minB[b2], 1.25*maxB[b2]))
  truehist(betas[,b3], xlim=c(1.25*minB[b3], 1.25*maxB[b3]))
  truehist(betas[,b4], xlim=c(1.25*minB[b4], 1.25*maxB[b4]))

  par(mfrow=c(1, 2))
  truehist(lamdas[,b1], xlim=c(1.25*minL, 1.25*maxL))
  truehist(sigmas[,b1], xlim=c(1.25*minS, 1.25*maxS))

}

plot.bayesOld <- function (results, pSampler = c(0)) {

  p <- dim(results$betas)[2]
  if (sum(pSampler) == 0) pSampler <- sample(1:p, 4)

  betas = results$betas[,pSampler]
  gammas = results$gammas[,pSampler]
  lamdas = results$lambdas
  sigmas = results$sigmas
  b1 = pSampler[1]; b2 = pSampler[2]; b3 = pSampler[3]; b4 = pSampler[4]
  minB = apply(betas, 2, min); maxB = apply(betas, 2, max)
  minL = min(lamdas); maxL = max(lamdas)
  minS = min(sigmas); maxS = max(sigmas)

  par(mfrow=c(2, 2))
  plot(betas[,b1], type="l", ylim=c(1.25*minB[b1], 1.25*maxB[b1]))
  plot(betas[,b2], type="l", ylim=c(1.25*minB[b2], 1.25*maxB[b2]))
  plot(betas[,b3], type="l", ylim=c(1.25*minB[b3], 1.25*maxB[b3]))
  plot(betas[,b4], type="l", ylim=c(1.25*minB[b4], 1.25*maxB[b4]))

  par(mfrow=c(1, 2))
  plot(lamdas[,b1], type="l", ylim=c(1.25*minL, 1.25*maxL))
  plot(sigmas[,b1], type="l", ylim=c(1.25*minS, 1.25*maxS))

}
