XplorerCRand <- function(y, X_full, c = 1, ld0, nIter, nGam, intercept = FALSE, by = 25) {

  eval(parse(text = InitVals_()))

  #X_full <- scale(X_full, center = F, scale = T) #/ log(n)

  kes <- 1:p
  for (i in 2:nIter) {

    dashboard(i, gammas, by)

    sigSq <- sigmaGen(a0, b0, y, X, B, n)
    sigmas[i] <- sigSq

    ld <- lambdaGen(ld, a1, b1, m, B, xtx, c, p_)
    lambdas[i] <- ld

    c <- cGen(c, xtx, B, m, ld)
    ciz[i] <- c

    ithGammas <- matrix(0, nGam, p)
    ithGammas[1,] <- gammas[i-1,]
    lp_prev <- logProgGamma(ld, sigSq, c, m, y, X, xtx, yy, xy)

    for (j in 2:nGam) {

      ithGammas[j,] <- ithGammas[j-1,]
      kes <- sample(kes,length(kes)) # Shuffle the variable keys
      for (k in kes) {
        curr <- ithGammas[j,k]
        gamma_alt <- ithGammas[j,]; gamma_alt[k] <- abs(1-curr)
        if(sum(gamma_alt) == 0) next

        X_alt <- selectX( X_full, gamma_alt); xtx_alt <- selectXtX(gamma_alt, XtX)
        m_alt <- eval_m(y, X_alt, xtx_alt, c); xy_alt <- selectXy(gamma_alt, Xy)
        lp_alt <- logProgGamma(ld, sigSq, c, m_alt, y, X_alt, xtx_alt, yy, xy_alt)
        log_r <- lp_alt - lp_prev

        val <- try(if (log_r > 0) {
          ithGammas[j,] <- gamma_alt
          lp_prev <- lp_alt
        })
        if (class(val)=="try-error") {
          befreq(gammas[1:i,])
          return("error")
        }
      }
    }

    igamma <- updatedGamma(ithGammas, nGam, gammas)
    gammas[i,] <- igamma
    if (nGam == 2) kes <- shrinker(gammas[1:i,kes], kes)


    # Generate beta | y, igamma, sigSq, ld
    index <- which(igamma == 1) #V <- sigSq*diag(n)
    X <- selectX(X_full, igamma); p_ <- dim(X)[2]; n_ = dim(X)[1]
    xtx <- selectXtX(igamma, XtX); m <- eval_m(y, X, xtx, c)
    xy <- selectXy(igamma, Xy)
    B <- betaGen(y, X, xtx, m, igamma, ld, c, sigSq, xy)


    betas[i, index] <- B
    residus[i] <- resEval(y - X %*% B) / n_
    probgams[i]<- logProgGamma(ld, sigSq, c, m, y, X, xtx, yy, xy)

  }
  res <- list(lambdas = lambdas, sigmas = sigmas, betas = betas, gammas = gammas, residus = residus,
              probgams = probgams, ciz = ciz)
  class(res) <- "bayes"
  res
}

# Helper Functions

cGen <- function(c, xtx, B, m, ld) {
  #b_ = 1; a_ = 1
  Cist <- function(c) {
    p = getDim(xtx)[1]
    #xtx = t(X) %*% X
    deter = det(xtx / c + diag(p))
    xp = -ld/c * phi(B - m, xtx, noInv = T)
    0.5*log(deter) + xp
  }
  sd = 0.1
  calcA <- function(c, sd) {(c / sd)^2}; calcB <- function(c, sd) {c / sd^2}
  c_ <- rgamma(1, shape = calcA(c, sd), rate = calcB(c,sd))
  propRatio <- dgamma(c, shape = calcA(c_, sd), rate = calcB(c_, sd)) / dgamma(c_, shape = calcA(c, sd), rate = calcB(c, sd))
  r <- exp(Cist(c_) - Cist(c)) * propRatio
  if (runif(1) < r) {c <- c_}
  #ciz[i] <- c
  c
}


shrinker <- function(gammas, kes, tol = 0.99) {
  if(is.null(dim(gammas))) { return(kes) }
  probs <- colMeans(gammas)
  pot <- probs > tol
  kes_new <- kes[pot]
  kes_new
}
sigmaGen <- function(a0, b0, y, X, B, n) {
  a_ <- a0 + n/2; b_ <- b0 + resEval(y - X %*% B) / 2
  sigSq <- 1/rgamma(n=1, shape = a_, rate = b_)
  sigSq
}

lambdaGen <- function(ld, a1, b1, m, B, xtx, c, p_) {
  a1_ <- a1 + p_/2
  M <- evalWould(c, xtx)
  b1_ <- b1 + phi(B - m, M, noInv = TRUE)
  ld <- rgamma(n=1, shape=a1_, rate = b1_)
  ld
}

dashboard <- function(i, gammas, by) {
  if (i %% by == 0) {
    print(c("iteration #", i))
    befreq(gammas[1:i,])
  }
}

updatedGamma <- function(ithGammas, nGam, gammas) {
  igamma <- as.numeric((colSums(ithGammas) / nGam > 0.5))
  if (sum(igamma) == 0) {
    print("wrong gamma")
    ind <- max(0,i - 1)
    igamma <- gammas[ind,] # sample(c(0,1), p, TRUE), 1:p
  }
  igamma
}

betaGen <- function(y, X, xtx, m, igamma, ld, c, sigSq, xy) {
  p_ <- dim(xtx)[2]; n_ = dim(X)[1]
  V <- sigSq*diag(n_)
  Winv <- evalWinv(c, xtx, ld)
  W_ <- inv(Winv + phi(X, V))
  m_ <- W_ %*% (Winv %*% m + xy/sigSq)
  B <- mvrnorm(1, m_, W_)
  B
}

##################################################3

library(MASS)
library(psych)
library(coda)
options(warn = -1)

InitVals_ <- function() {
  " n <- dim(X_full)[1];
  if (intercept) { X_full <- cbind(rep(1,n), X_full) }
  p <- dim(X_full)[2]; XtX <- t(X_full) %*% X_full
  Xy = t(X) %*% y
  betas <- matrix(0, nIter, p)
  gammas <- matrix(1, nIter, p)
  sigmas <- matrix(0, nIter, 1)
  lambdas <- matrix(0, nIter, 1)
  residus <- matrix(NA, nIter, 1)
  probgams <- matrix(NA, nIter, 1)
  ciz <- matrix(NA, nIter, 1)


  a0 <- 0.0; b0 <- a0; a1 <- a0; b1 <- a1
  m0 <- eval_m(y, X_full, XtX, c)

  sigSq0 <- var(y)
  gamma0 <- rep(1,p)
  W0 = inv(evalWinv(c, XtX, ld0))
  B0 <- mvrnorm(1, m0, W0);
  sigmas[1] <- sigSq0;
  lambdas[1] <- ld0;
  betas[1,] <- B0;
  gammas[1,] <- gamma0;
  ciz[1] <- c

  X <- X_full; B <- B0
  m <- m0; p_ <- p
  ld <- ld0; xtx <- XtX
  xy = Xy ; yy = resEval(y) "
}

dataloader_ <- function() {
  "y = data$y
  X = data$X
  betas = data$betas
  tgs = data$tgs"
}

inv <- function(x) { solve(x) } #inverse

evalWinv <- function(c, xtx, ld) {
  p <- getDim(xtx)[2]
  res <- ld * xtx / c + ld * diag(p)
  res
}

evalWould <- function(c, xtx) {
  p <- getDim(xtx)[2]
  res <- xtx / c +  diag(p)
  res
}

eval_m <- function(y, X, xtx, c) {
  p = getDim(xtx)[2]
  inv(xtx / c + diag(p)) %*% t(X) %*% y # xtx / c + diag(p)
}

resEval <- function(u) {
  t(u) %*% (u)
}

phi <- function(u, M, v = "", noInv = FALSE) {
  check = FALSE
  if (length(v) == 1) check = v == ""
  if (check & !noInv) { t(u) %*% inv(M) %*% u }
  else if (check & noInv) { t(u) %*% M %*% u }
  else if (noInv) { t(u) %*% M %*% v}
  else {  t(u) %*% inv(M) %*% v }
}


logProgGamma <- function(ld, sigSq, c, m, y, X, xtx, yy, xy) {
  n <- getDim(X)[1]; Winv <- evalWinv(c, xtx, ld)
  W <- inv(Winv); W_ <- inv(Winv +  xtx/sigSq)
  m_ <- W_ %*% (Winv %*% m + xy/sigSq)
  le = -0.5 * (-phi(m_, W_) + yy/sigSq + phi(m, Winv, noInv = TRUE))
  lp <- -n/2 * sigSq + 0.5*(log(det(Winv))+ log(det(W_))) + le
}

selectX <- function(X, gamma) {
  if (sum(gamma) == dim(X)[2]) X
  else if (sum(gamma) == 1) as.matrix(X[,which(gamma == 1)])
  else if (sum(gamma) == 0) { print("Selection Error: No variables selected") }
  else { X[,which(gamma == 1)] }
}

selectXtX <- function(gamma, XtX) {
  index = which(gamma == 1)
  if (sum(gamma) == dim(XtX)[1]) XtX
  else if (sum(gamma) == 1) as.matrix(XtX[index,index])
  else if (sum(gamma) == 0) { print("Selection Error for XtX: No correlations selected") }
  else { XtX[index,index] }
}

selectXy <- function(gamma, Xy) {
  index = which(gamma == 1)
  if (sum(gamma) == dim(Xy)[1]) Xy
  else if (sum(gamma) == 1) as.matrix(Xy[index,])
  else if (sum(gamma) == 0) { print("Selection Error for Xy: No correlations selected") }
  else { Xy[index,] }
}

getDim <- function(X) {
  d <- dim(X)
  n = if (is.null(d)) {length(X)} else d[1]
  p = if (is.null(d)) {1} else d[2]
  c(n,p)
}

