---
title: "Testing"
output: html_notebook
---

# Example 1.1
```{r warning = FALSE}
nn = 1000; se = 1; num = 100; 
argz = list(d = 1, ld0 = 1, nIter = nn, nGam = 2, by = nn)
rez1 = repeatExp("Fayda", argz, p = 8,  n = 60, sig = 3, tgs, num = num, se = se) 
```

```{r warning = FALSE}
nn = 10; nReap = 20;se = 1; num = 100; 
argz = list(c = 1, ld0 = 1/20, nIter = nn, nGam = 2, by = nn, adapt = T)#, nReap = nReap)
rez1 = repeatExp("XplorerC", argz, p = 8,  n = 30, sig = 1, tgs, num = num, se = se) 
```

```{r warning = FALSE}
nn = 10; nReap = 20;se = 1; num = 100; 
argz = list(c = 1, ld0 = 1/49, nIter = nn, nGam = 2, by = nn,adapt = F)#, nReap = nReap)
rez1 = repeatExp("XplorerC", argz, p = 8,  n = 60, sig = 1, tgs, num = num, se = se) 
```

```{r warning = FALSE}
nn = 10; nReap = 20;se = 1; num = 100; 
argz = list(c = 1, ld0 = 1/80, nIter = nn, nGam = 2, by = nn, adapt = F)#, nReap = nReap)
rez1 = repeatExp("XplorerC", argz, p = 8,  n = 120, sig = 1, tgs, num = num, se = se) 
```

```{r warning = FALSE}
nn = 50; nReap = 20;se = 1; num = 100; 
argz = list(c = 1/1, ld0 = 1/10, nIter = nn, nGam = 2, by = nn, adapt = T)#, nReap = nReap)
rez1 = repeatExp("XplorerC", argz, p = 8,  n = 30, sig = 3, tgs, num = num, se = se) 
```

```{r warning = FALSE}
nn = 500; nReap = 20;se = 1; num = 100; 
argz = list(c = 1, ld0 = 1/40, nIter = nn, nGam = 2, by = nn, adapt = F)#, nReap = nReap)
rez1 = repeatExp("XplorerC", argz, p = 8,  n = 60, sig = 3, tgs, num = num, se = se) 
```


``````{r warning = FALSE}
nn = 10; nReap = 20;se = 1; num = 100; 
argz = list(c = 1, ld0 = 1/200, nIter = nn, nGam = 2, by = nn,adapt = F)#, nReap = nReap)
rez1 = repeatExp("XplorerC", argz, p = 8,  n = 120, sig = 3, tgs, num = num, se = se) 
```


######### ExampleTwo ##########


```{r warning = FALSE}
nn = 10; nReap = 20;se = 1; num = 100; 
argz = list(c = 2, ld0 = 1/40, nIter = nn, nGam = 2, by = nn, adapt = F)#, nReap = nReap)
rez1 = repeatExp("XplorerC", argz, p = 4,  n = 60, sig = 9, tgs, num = num, se = se) 
```

```{r warning = FALSE}
nn = 10; nReap = 20;se = 1; num = 100; 
argz = list(c = 2, ld0 = 1/80, nIter = nn, nGam = 2, by = nn, adapt = T)#, nReap = nReap)
rez1 = repeatExp("XplorerC", argz, p = 4,  n = 120, sig = 5, tgs, num = num, se = se) 
```

```{r warning = FALSE}
nn = 10; nReap = 20;se = 1; num = 100; 
argz = list(c = 2, ld0 = 1/160, nIter = nn, nGam = 2, by = nn, adapt = T)#, nReap = nReap)
rez1 = repeatExp("XplorerC", argz, p = 4,  n = 300, sig = 3, tgs, num = num, se = se) 
```


```{r warning = FALSE}
nn = 10; nReap = 20;se = 1; num = 100; 
argz = list(c = 2, ld0 = 1/80, nIter = nn, nGam = 2, by = nn, adapt = T)#, nReap = nReap)
rez1 = repeatExp("XplorerC", argz, p = 4,  n = 300, sig = 1, tgs, num = num, se = se) 
```

######### ExampleThree ##########
```{r}
nn = 100; nReap = 20;se = 1; num = 100; 
argz = list(c = 1, ld0 = 1/10, nIter = nn, nGam = 2, by = nn,adapt = T) #, nReap = nReap)
rez1 = repeatExp("XplorerC", argz, p = 100,  n = 50, sig = 1, tgs, num = num, se = se) 
```


```{r warning = FALSE}
nn = 100; nReap = 20;se = 1; num = 100; 
argz = list(c = 1, ld0 = 1/50, nIter = nn, nGam = 2, by = nn,adapt = T) #, nReap = nReap)
rez1 = repeatExp("XplorerC", argz, p = 100,  n = 50, sig = 3, tgs, num = num, se = se) 
```


```{r}
nn = 100; nReap = 20;se = 1; num = 10; 
argz = list(c = 1, ld0 = 1/70, nIter = nn, nGam = 2, by = nn,adapt = T) #, nReap = nReap)
rez1 = repeatExp("XplorerC", argz, p = 100,  n = 100, sig = 5, tgs, num = num, se = se) 
```







```{r}
argz = list("sig" = 1, "n" = 30, "c" = 1, "ld" = 1/1, "seed" = 3); nn = 1000
genArgz = list(nIter = nn, nGam = 20, by = nn)
resDf = jumpsDisplay(exampleOne, argz, XplorerC, genArgz)
resDf
```



```{r warning = FALSE}
rez1 = repeatExp("XplorerC", argz, tgs, num = num, se = se, p = 8) 
```

```{r warning = FALSE}
rez1 = repeatExp("XplorerCnoCentered", argz, tgs, num = num, se = se, p = 8) 
```

# Example 2.1

```{r warning = FALSE}
sig = 1
data = exampleTwo(sig = sig,  n = 30)
eval(parse(text = dataloader_()))
# c = 3.58, 3.60 not 3.59
nn = 10; se = 1; num = 10
argz = list(y = y, X_full = X, c = 1, ld0 = 1/1000, nIter = nn, nGam = 2, by = nn)
rez1 = repeatExp("XplorerC", argz, tgs, num = num, se = se, p = 4) 
```


```{r warning = FALSE}
rez2 = repeatExp("XplorerC", argz, tgs, num = num, se = se, p = 4) 
```


# Example 3.1

```{r warning = FALSE}
data = exampleThree(sig = 5,  n = 100)
eval(parse(text = dataloader_()))
# c = 3.58, 3.60 not 3.59
nn = 3000; se = 9; num = 1
argz = list(y = y, X_full = X, c = 1, ld0 = 1/1000, nIter = nn, nGam = 3, by = 500)
rez3 = repeatExp("XplorerCRand", argz, tgs, num = num, se = se, p = 100) 
```

```{r warning = FALSE}
rez = repeatExp("XplorerC", argz, tgs, num = num, se = se, p = 100) 
```









################     #################    #################






# Example 1.2

```{r warning = FALSE}
data = exampleOne(sig = 1,  n = 30)
eval(parse(text = dataloader_()))
# c = 3.58, 3.60 not 3.59
nn = 1000; num = 10; se = 1
argz = list(y = y, X_full = X, c = 1, ld0 = 0.001, nIter = nn, nGam = 2, by = nn)
```

```{r}
data = exampleZero(sig = 9,  n = 120)
eval(parse(text = dataloader_()))
# c = 3.58, 3.60 not 3.59
nn = 100; num = 10; se = 1
argz = list(y = y, X_full = X, c = 1, ld0 = 0.001, nIter = nn, nGam = 2, by = nn)
rez = repeatExp("XplorerC", argz, tgs, num = num, se = se, p = 5) 
```


```{r warning = FALSE}
#rezc = repeatExp("XplorerC", argz, tgs, num = num, se = se, p = 8) 
res = do.call(XplorerC, argz)
tail(res$allGammas,20); tail(res$accepteds, 20)
round(colMeans(res$allGammas), 3)
plot(res$allGammas[1500:1520,9])
f = VarInspect(exampleTwo, list("sig" = c(1,3,5,9), "n" = 30, "c" = 1), "sig", plotIt = T)
binReduce(res$allGammas[,-5])
```

```{r warning = FALSE}
rezcno = repeatExp("XplorerCnoCentered", argz, tgs, num = num, se = se, p = 8)
```

```{r warning = FALSE}
rezcno10 = repeatExp("XplorerCnoCenteredzr", argz, tgs, num = num, se = se, p = 8)
```


# Example 2.2

```{r warning = FALSE}
data = exampleTwo(sig = 9,  n = 60)
eval(parse(text = dataloader_()))
# c = 3.58, 3.60 not 3.59
nn = 250; num = 10; se = 1
argz = list(y = y, X_full = X, c = 1, ld0 = 0.001, nIter = nn, nGam = 2, by = nn)
```

```{r}
res = do.call(XplorerC, argz)
traceplot(as.mcmc(res$sigmas))
traceplot(as.mcmc(res$lambdas)) # mean(res$sigmas[10:1000])
autocorr.plot(as.mcmc(res$sigmas))
autocorr.diag(as.mcmc(res$lambdas))
effectiveSize(as.mcmc(res$betas))
raftery.diag(as.mcmc(res$betas))
res$gammas[res$lambdas > 10**5,]
t(x) %*% y / t(x) %*% x
```


```{r warning = FALSE}
rez = repeatExp("XplorerCvy", argz, tgs, num = num, se = se, p = 4) 
```

```{r warning = FALSE}
rez = repeatExp("XplorerC", argz, tgs, num = num, se = se, p = 4) 
```

```{r warning = FALSE}
rez = repeatExp("XplorerCnoCentered", argz, tgs, num = num, se = se, p = 4)
```

```{r warning = FALSE}
rez = repeatExp("XplorerCnoCenteredzr", argz, tgs, num = num, se = se, p = 4)
```


# Example 3.2

```{r warning = FALSE}
data = exampleThree(sig = 3,  n = 50)
eval(parse(text = dataloader_()))
# c = 3.58, 3.60 not 3.59
nn = 250; num = 10; se = 1
argz = list(y = y, X_full = X, c = 1, ld0 = 1, nIter = nn, nGam = 2, by = nn)
```


```{r warning = FALSE}
rez = repeatExp("XplorerCvy", argz, tgs, num = num, se = se, p = 100) 
```

```{r warning = FALSE}
rez = repeatExp("XplorerC", argz, tgs, num = num, se = se, p = 100) 
```

```{r warning = FALSE}
rez = repeatExp("XplorerCnoCentered", argz, tgs, num = num, se = se, p = 100)
```

```{r warning = FALSE}
rez = repeatExp("XplorerCnoCenteredzr", argz, tgs, num = num, se = se, p = 100)
```

# Example 3.3

```{r warning = FALSE}
data = exampleThree(sig = 5,  n = 200)
eval(parse(text = dataloader_()))
# c = 3.58, 3.60 not 3.59
nn = 1000; num = 10; se = 1
argz = list(y = y, X_full = X, c = 1, ld0 = 1, nIter = nn, nGam = 2, by = nn)
```

```{r warning = FALSE}
rez = repeatExp("XplorerCvy", argz, tgs, num = num, se = se, p = 100)
```

```{r warning = FALSE}
rez = repeatExp("XplorerC", argz, tgs, num = num, se = se, p = 100)
```

```{r warning = FALSE}
rez = repeatExp("XplorerCnoCentered", argz, tgs, num = num, se = se, p = 100)
```

```{r warning = FALSE}
rez = repeatExp("XplorerCnoCenteredzr", argz, tgs, num = num, se = se, p = 100)
```

